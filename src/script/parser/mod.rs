use std::io::{Read, Error as IoError};

mod stream;

pub struct ScriptParser<S>
where S: Read {
    inner: S,
}

impl<S> ScriptParser<S>
where S: Read {
    pub fn new(stream: S) -> ScriptParser<S> {
        ScriptParser {
            inner: stream,
        }
    }
}