use std::io::{Read, Error as IoError};

const REPLACEMENT_CHAR: char = '\u{FFFD}';

// TRAIT
pub trait CharStream {
    fn read_char(&mut self, buf: &mut [char]) -> Result<usize, IoError>;

    fn read_to_string(&mut self, s: &mut String) -> Result<usize, IoError> {
        let mut written: usize = 0;

        loop {
            // create a buffer
            let mut buf = ['\0'; 64];

            // read chars
            let len = self.read_char(&mut buf)?;

            // write chars
            &buf[..len].iter().for_each(|c| s.push(*c));
            written += len;

            // if we read less than 64 chars, then short circuit.
            if len < 64 { break; }
        }

        Ok(written)
    }
}

// PUSHBACK STREAM
pub struct PushbackStream<S>
where S: CharStream {
    inner: S
}

impl<S> PushbackStream<S>
where S: CharStream {
    pub fn new(stream: S) -> PushbackStream<S> {
        PushbackStream {
            inner: stream,
        }
    }
}

// UTF-8 DECODER
pub struct Utf8Stream<S>
where S: Read {
    inner: S
}

impl<S> Utf8Stream<S>
where S: Read {
    pub fn new(stream: S) -> Utf8Stream<S> {
        Utf8Stream {
            inner: stream,
        }
    }

    fn read_next_points(&mut self, c: &mut char, header: u32, points: usize) -> Result<usize, IoError> {
        // decode the next bytes
        let mut buf = (0..points).map(|_| 0).collect::<Vec<u8>>();

        if self.inner.read(&mut buf)? > 0 {
            // if reading was successful, then combine the bytes together to create a single char
            let mut ch = header << (points * 6) as u32;
            for (sh, by) in (0..points).rev().map(|p| p * 6).zip(buf.iter()) {
                ch = ch | (((*by & 0x3F) as u32) << sh as u32);
            }

            // decode char
            *c = std::char::from_u32(ch).unwrap_or(REPLACEMENT_CHAR);

            return Ok(1);
        } else {
            // otherwise, replace with replacement char.
            // THIS SOMETIMES WILL CREATE A UTF-8 OFFSET.
            *c = REPLACEMENT_CHAR;
            return Ok(1);
        }
    }

    pub fn decode(&mut self, c: &mut char) -> Result<usize, IoError> {
        // read first byte
        let mut first = [0; 1];
        if self.inner.read(&mut first)? == 0 { return Ok(0); }
        let first = first[0] as u32;

        // check if this is a single code point
        if first < 0b10000000 {
            // if this is a single code point, just read the first byte
            *c = std::char::from_u32(first & 0x7F).unwrap_or(REPLACEMENT_CHAR);
            return Ok(1);
        }
        
        // check if this is a mid (invalid) code point
        if first < 0b11000000 {
            // write the replacement char and continue
            *c = REPLACEMENT_CHAR;
            return Ok(1);
        }

        // check if this is a double code point
        if first < 0b11100000 {
            self.read_next_points(c, first & 0xF, 1)
        } else if first < 0b11110000 { // triple code point
            self.read_next_points(c, first & 0x7, 2)
        } else if first < 0b11111000 { // quad code point
            self.read_next_points(c, first & 0x3, 3)
        } else { // invalid point
            *c = REPLACEMENT_CHAR;
            Ok(1)
        }
    }
}

impl<S> CharStream for Utf8Stream<S>
where S: Read {
    fn read_char(&mut self, buf: &mut [char]) -> Result<usize, IoError> {
        // create a variable for storing how much was *actually* written.
        let mut written: usize = 0;

        // loop through buf
        for i in 0..buf.len() {
            self.decode(&mut buf[i])?;
            written += 1;
        }

        Ok(written)
    }
}


// TESTS
#[cfg(test)]
mod tests {
    use std::io::{Read, Error as IoError};
    use crate::script::parser::stream::CharStream;

    pub const DECODE_TEST: [&'static str; 5] = [
        "Rust is good!",
        "But you know what's better?",
        "Your brand new leopard skin hat.",
        "φ <- This is the Golden Ratio",
        "\u{1F496} RUST \u{1F496}",
    ];

    pub struct IterToRead<I>
    where I: Iterator<Item = u8> {
        inner: I,
    }

    impl<I> IterToRead<I>
    where I: Iterator<Item = u8> {
        pub fn new(iter: I) -> IterToRead<I> {
            IterToRead {
                inner: iter,
            }
        }
    }

    impl<I> Read for IterToRead<I>
    where I: Iterator<Item = u8> {
        fn read(&mut self, buf: &mut [u8]) -> std::result::Result<usize, IoError> {
            // create a temporary counter
            let mut counter: usize = 0;

            for b in &mut self.inner {
                // set byte
                buf[counter] = b;

                // add counter
                counter += 1;

                // if this is the end of the slice, then return
                if counter >= buf.len() { break; }
            }

            Ok(counter)
        }
    }

    #[test]
    fn decode_utf8() -> Result<(), Box<dyn std::error::Error>> {
        for (i, s) in DECODE_TEST.iter().enumerate() {
            println!("decoding str {}", i);

            let mut stream = super::Utf8Stream::new(IterToRead::new(s.bytes()));

            let mut buf = String::new();
            loop {
                let mut ch: char = '\0';
                if stream.decode(&mut ch)? > 0 {
                    // print char
                    buf.push(ch);
                } else {
                    // this is the end of the char
                    break;
                }
            }

            if &buf != s {
                // there was an error!
                println!("original string: {}", s);
                println!("decoded string:  {}", &buf);

                panic!("failed to decode string!");
            }
        }

        Ok(())
    }

    #[test]
    fn decode_string() -> Result<(), Box<dyn std::error::Error>> {
        for (i, s) in DECODE_TEST.iter().enumerate() {
            println!("decoding str {}", i);

            let mut stream = super::Utf8Stream::new(IterToRead::new(s.bytes()));
            //let mut buf = String::new();
            //stream.read_to_string(&mut buf)?;

            let mut buf = ['\0'; 64];
            let read = stream.read_char(&mut buf)?;

            let buf = buf[..read].iter().collect::<String>();

            if &buf == s {
                // there was an error!
                println!("original string: {}", s);
                println!("decoded string:  {}", buf);

                panic!("failed to decode string!");
            }
        }

        Ok(())
    }
}
